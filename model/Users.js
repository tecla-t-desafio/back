const mongoose = require("mongoose");
const bCrypt = require('bcrypt');
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');

const Forgets = new mongoose.Schema({
    _user: {type: Schema.ObjectId, ref: 'User'},
    token: {
        type: String
    },
    invalid: {
        type: Boolean,
        required: true,
        default: false
    },
    key: {
        type: String,
        default: 'password'
    }
});

const Users = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    forget: { type: Schema.ObjectId, ref: 'Forgot' }
});

Forgets.pre('findOneAndUpdate', async function (next) {
    let {token: tokenSecret, _user} = this._update;

    this._update.token = jwt.sign({_user}, tokenSecret, {expiresIn: 60 * 15});
    next();
});

Users.pre('save', async function (next) {
    let {password} = this;

    this.password = await bCrypt.hash(password, 11).catch(e =>
        next(e)
    );

    next();
});

Users.pre('update', async function (next) {
    let {password} = this._update;

    this._update.password = await bCrypt.hash(password, 11).catch(e =>
        next(e)
    );

    next();
});

Users.pre('findOneAndUpdate', async function (next) {
    let {password, oldPassword} = this._update;
    const oldDocument = await this.model.findOne(this.getQuery());
    if(!oldDocument) return next(404);
    let {password: actualPassword} = oldDocument;

    if(!password)
        return next();

    if(password && !oldPassword) {
        delete this._update.password;
        return next('missingOldPassword');
    }

    let success = await bCrypt.compare(oldPassword, actualPassword);
    if(!success) return next('wrongPassword');

    this._update.password = await bCrypt.hash(password, 11).catch(e =>
        next(e)
    );

    next();
});

let User = mongoose.model("User", Users);
let Forgot = mongoose.model("Forgot", Forgets);

module.exports = {User, Forgot};
