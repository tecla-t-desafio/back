const jwt = require('jsonwebtoken');
const {TOKEN_SECRET} = process.env;

module.exports = (req, res, next) => {
    let {authorization} = req.headers;

    try {
        authorization = authorization.substr(7, authorization.length-1);
        jwt.verify(authorization, TOKEN_SECRET);
    }catch (e) {
        return res.status(401).json({message: 'Unauthorized'})
    }

    next();

}