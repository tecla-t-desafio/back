let users = require('./view/users');
let auth = require('./view/auth');

module.exports = [
    users,
    auth
];