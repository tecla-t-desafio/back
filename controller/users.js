const {User} = require('../model/Users');

function create(req, res){
    let {body: data} = req;

    createUser(data).then((data) => res.user.show(data, 201)).catch(res.user.error);
}

function remove(req, res){
    let {id} = req.params;

    removeUser(id).then(res.user.show).catch(() => res.user.error(404));
}

function update(req, res){
    let {id} = req.params;
    let {body: data} = req;

    updateUser(id, data).then(res.user.show).catch(res.user.error);
}

function read(req, res){
    let {id} = req.params;

    getUser(id).then(res.user.show).catch(res.user.error);
}

function list(req, res){
    let {query} = req;

    getUserList(query).then(res.user.list).catch(res.user.error);
}

module.exports = {
    create,
    remove,
    update,
    read,
    list,
    getUserByEmail,
    createUser
};

async function createUser(data){
    let user = new User(data);

    await user.save();
    if(!user) return Promise.reject(500);
    return user;
}
async function removeUser(id){
    let user = await User.findByIdAndDelete(id);

    if(!user) return Promise.reject(404);
    return user;
}
async function updateUser(id, data){
    let user = await User.findByIdAndUpdate(id, data, {new: true});

    if(!user) return Promise.reject(404);
    return user;
}
async function getUser(id){
    let user = await User.findById(id);

    if(!user) return Promise.reject(404);
    return user;
}
async function getUserByEmail(email){
    let user = await User.findOne({email});

    if(!user) return Promise.reject(404);
    return user;
}
async function getUserList (queryData, customQuery = {}) {
    let { page, perPage, field, order, q, qField } = queryData;

    let query = q ? {
        ...customQuery,
        [qField || 'name']: {
            $regex: q,
            $options: "i"
        }
    } : customQuery;

    if (!page) page = 1;
    if (!perPage) perPage = 0;
    if (!field) field = 'name';

    let total = await User.countDocuments(query);
    let list = await User.find(query).limit(parseInt(perPage)).skip(perPage * (page - 1)).sort({[field]: order});

    return { list, total };
}
