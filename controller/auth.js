const jwt = require('jsonwebtoken');
const users = require('../controller/users');
const nodeMailer = require('nodemailer');
const bCrypt = require('bcrypt');
const {random} = require('../utils');

const {User, Forgot} = require('../model/Users');

const {TOKEN_SECRET, SMTP: host, EMAIL: user, EMAIL_PASSWORD: pass} = process.env;

const transporter = nodeMailer.createTransport({
    host,
    port: 587,
    secure: false,
    auth: {
        user,
        pass
    }
});
transporter.verify((error, success) => { if (error) throw new Error(error) });

function login(req, res){
    let {email, password} = req.body;

    loginAction(email, password).then(res.auth.show).catch(res.auth.error);
}

function forgot(req, res){
    let {email} = req.body;

    createToken(email).then(res.auth.forgot.show).catch(res.auth.error);
}

function forgotCheck(req, res){
    let {token, email} = req.body;

    checkIfTokenIsValid(email, token).then(res.auth.forgot.show).catch(e => res.auth.error('tokenInvalid', e));
}

function useToken(req, res){
    let {token, email, password} = req.body;

    updatePassword(email, token, password).then(res.auth.forgot.show).catch(e => res.auth.error('tokenInvalid', e));
}

module.exports = {
    login,
    forgot,
    forgotCheck,
    useToken
};

async function loginAction(email, password){
    let user = await User.findOne({email});
    if(!user) return Promise.reject(404);

    let success = await bCrypt.compare(password, user.password);
    if(!success) return Promise.reject(403);

    let {email: userEmail, _id: id} = user;

    let token = jwt.sign({email: userEmail, id}, TOKEN_SECRET);

    return {id, token};
}

async function createToken(email){
    let user = await users.getUserByEmail(email);

    let token = random(6);
    let forgot = await Forgot.findOneAndUpdate({_user: user._id},{token, invalid: false}, { upsert: true, new: true, setDefaultsOnInsert: true });

    if(!forgot) return Promise.reject(500);

    let message = {
        from: 'Tecla T',
        to: email,
        subject: "Recuperar senha",
        text: `Seu código de recuperação: ${token}`,
        html: `<p>Seu codigo de recuperação: ${token}</p>`
    };

    await transporter.sendMail(message);

    return forgot;
}

async function checkIfTokenIsValid(email, token){
    let user = await users.getUserByEmail(email);

    let forgot = await findForgot({_user: user.id, invalid: false});

    try{
        jwt.verify(forgot.token, token);
    }catch (e) {
        return Promise.reject(403);
    }

    return forgot;
}

async function updatePassword(userEmail, token, password){
    let user = await users.getUserByEmail(userEmail);

    let forgot = await findForgot({_user: user.id, invalid: false});

    try {
        jwt.verify(forgot.token, token);
    }catch (e) {
        return Promise.reject(403);
    }

    await Forgot.update({_id: forgot.id}, {invalid: true}, {new: true});
    await User.update({_id: user._id}, {password});

    return forgot;
}

async function findForgot(query){
    let forgot = await Forgot.findOne(query).populate('_user');

    if(!forgot) return Promise.reject(403);

    return forgot;
}