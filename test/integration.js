require('dotenv').config();
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);
const {random} = require('../utils');

let {SERVER_TEST, HOST_TEST} = process.env;
SERVER_TEST = SERVER_TEST === 'true';

const host = HOST_TEST;
const testUser = {name: 'teste', email: 'test@test.test', password: 'teste123'};
let userData;

let randomNumber = random(10);
let randomUser = ({name: `User ${randomNumber}`, email: `user${randomNumber}@test.test`, password: 'test123'});

describe('Users', async () => {
    userData = await new Promise(resolve => {
        describe('/POST Auth', async () => {
            it('Authing', (done) => {
                chai.request(host)
                    .post('/auth')
                    .set('content-type', 'application/json')
                    .send(testUser)
                    .end((err, res) => {
                        res.should.have.status(200);
                        resolve(res.body.data);
                        done();
                    });
            })

            it('User not found', (done) => {
                chai.request(host)
                    .post('/auth')
                    .set('content-type', 'application/json')
                    .end((err, res) => {
                        res.should.have.status(404);
                        done();
                    });
            });

            it('Password empty', (done) => {
                chai.request(host)
                    .post('/auth')
                    .set('content-type', 'application/json')
                    .send({email: testUser.email})
                    .end((err, res) => {
                        res.should.have.status(400);
                        done();
                    });
            });

            it('Wrong password', (done) => {
                chai.request(host)
                    .post('/auth')
                    .set('content-type', 'application/json')
                    .send({email: testUser.email, password: 'abc'})
                    .end((err, res) => {
                        res.should.have.status(401);
                        done();
                    });
            });
        })
    });
    describe('/GET [LIST]', async () => {
        it('Not authorized', (done) => {
            chai.request(host)
                .get('/users')
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });

        it('Pagination', (done) => {
            chai.request(host)
                .get('/users')
                .query({page: 2, perPage: 10})
                .set('authorization', `Bearer ${userData.token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/GET [ONE]', async () => {
        it('Not authorized', (done) => {
            chai.request(host)
                .get(`/users/${userData.id}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
        it('Success one', (done) => {
            chai.request(host)
                .get(`/users/${userData.id}`)
                .set('authorization', `Bearer ${userData.token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it('Not found', (done) => {
            chai.request(host)
                .get(`/users/5ef803c0dbc7100692d7e429`)
                .set('authorization', `Bearer ${userData.token}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('/PUT [ONE]', async () => {
        it('Not authorized', (done) => {
            chai.request(host)
                .put(`/users/${userData.id}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
        it('Success one', (done) => {
            chai.request(host)
                .put(`/users/${userData.id}`)
                .set('authorization', `Bearer ${userData.token}`)
                .send({name: 'Usuario de testes'})
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it('Bad request', (done) => {
            chai.request(host)
                .put(`/users/5ef803c0dbc7100692d7e429`)
                .set('authorization', `Bearer ${userData.token}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it('Not found', (done) => {
            chai.request(host)
                .put(`/users/5ef803c0dbc7100692d7e429`)
                .set('authorization', `Bearer ${userData.token}`)
                .send({name: "testando 123"})
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('/DELETE', async () => {
        it('Not authorized', (done) => {
            chai.request(host)
                .delete(`/users/${userData.id}`)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });

        if (SERVER_TEST)
            it('Success one', (done) => {
                chai.request(host)
                    .delete(`/users/${userData.id}`)
                    .set('authorization', `Bearer ${userData.token}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });

        it('Not found', (done) => {
            chai.request(host)
                .delete(`/users/5ef803c0dbc7100692d7e429`)
                .set('authorization', `Bearer ${userData.token}`)
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });

    describe('/POST [ONE]', async () => {
        it('Bad request', (done) => {
            chai.request(host)
                .post(`/users/`)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
        it('Success one', (done) => {
            chai.request(host)
                .post(`/users/`)
                .send(randomUser)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                });
        });
    });
});
