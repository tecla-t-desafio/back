require('./app');

let usersController = require('./controller/users');

const {random} = require('./utils');

let testUser = {name: 'teste', email: 'test@test.test', password: 'teste123'};

let users = new Array(99).fill({});
users = users.map(() => {
    let randomNumber = random(10);
    return ({name: `User ${randomNumber}`, email: `user${randomNumber}@test.test`, password: 'test123'});
});

users = [testUser, ...users];

(async () => {
    for(let user of users) await usersController.createUser(user).catch(e => console.log(e));
    console.log('Db seeded!')
    process.exit(0);
})();