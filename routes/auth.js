const express = require('express');
const routes = express.Router();
const auth = require('../controller/auth');

routes.post('/', auth.login);

routes.get('/forgot', auth.forgotCheck);
routes.put('/forgot', auth.useToken);
routes.post ('/forgot', auth.forgot);

module.exports = routes;