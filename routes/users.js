const express = require('express');
const routes = express.Router();
const user = require('../controller/users');
const authMiddleware = require('../middlewares/auth');

routes.post('/', user.create);

routes.use(authMiddleware);
routes.get('/:id', user.read);
routes.put('/:id', user.update);
routes.delete('/:id', user.remove);
routes.get('/', user.list);

module.exports = routes;