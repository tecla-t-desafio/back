module.exports = {
    random: (n) => (new Array(n).fill(0).map(() => Math.floor((Math.random()*8)+1))).join('')
}