const express = require('express');
const routes = express.Router();

const user = require('./routes/users');
const auth = require('./routes/auth');

routes.use('/users', user);
routes.use('/auth', auth);

module.exports = routes;