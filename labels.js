module.exports = {
    'pt-BR': {
        users: {
            '404': {
                message: 'Usuário não encontrato',
                status: 404
            },
            '401': {
                message: 'Você não está autenticado para realizar esta ação',
                status: 401
            },
            '403': {
                message: 'Autenticação falhou',
                status: 403
            },
            '500': {
                message: 'Ocorreu um erro interno',
                status: 500
            },
            '11000': {
                message: 'Já existe um usuário cadastrato com os mesmos dados',
                status: 400
            },
            valueMissing: {
                message: 'Os parametros obrigatórios não foram preenchidos',
                status: 400
            },
            missingOldPassword: {
                message: 'Para alterar a senha você não pode deixar o campo de senha antiga em branco',
                status: 400
            },
            wrongPassword: {
                message: 'Senha incorreta',
                status: 401
            }
        },
        auth: {
            '404': {
                message: 'Usuário não encontrato',
                status: 404
            },
            '403': {
                message: 'A senha está incorreta',
                status: 401
            },
            '401': {
                message: 'Você não está autenticado para realizar esta ação',
                status: 401
            },
            '500': {
                message: 'Ocorreu um erro interno',
                status: 500
            },
            'tokenInvalid': {
                message: 'Token inserido é inválido',
                status: 403
            }
        }
    }
}