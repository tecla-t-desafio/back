let labels = require('../labels');

module.exports = (req, res, next) => {
    res.auth = {
        show: (data, status = 200) => {
            let {id, token} = data;

            return res.status(status).json({
                data: {
                    id,
                    token
                }
            });
        },
        forgot: {
            show: (data, status = 200) => {
                let {id ,invalid} = data;

                res.status(status).json({data: {id, invalid}})
            }
        },
        error(key ,e) {
            console.log(e || key);

            let error = {message: 'Falha desconhecida', ...(key instanceof Object ? {error: key} : {})}, status = 400;

            let {'accept-language': language} = req.headers;
            if (language) language = language.split(',').filter(item => labels[item])[0];

            let userLabels = labels[language || 'pt-BR'].auth;
            if (key.message)
                if (key.message.includes('required'))
                    key = 'valueMissing';

            if (key instanceof Object) key = key.code;

            let label = userLabels[key];

            if (label) {
                error.message = label.message;
                status = label.status;
            }

            res.status(status).json(error);
        }
    };

    next();
}