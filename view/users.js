let labels = require('../labels');

module.exports = (req, res, next) => {
    res.user = {
        show: (data, status = 200) => {
            let {_id: id, email, name} = data;

            return res.status(status).json({
                data: {
                    id,
                    name,
                    email,
                }
            });
        },
        list: ({list, total}) => {
            return res.status(200).json({
                data: list.map(({_id: id, name, email}) => ({id, name, email})),
                total
            });
        },
        error(key){
            console.log('[USERS] -',key);

            let error = {message: 'Falha desconhecida', ...(key instanceof Object ? {error: key} : {})}, status=400;

            let {'accept-language': language} = req.headers;
            if(language) language = language.split(',').filter(item => labels[item])[0];

            let userLabels = labels[language || 'pt-BR'].users;
            if(key.message)
                if(key.message.includes('required'))
                    key = 'valueMissing';

            if(key instanceof Object) key = key.code;

            let label = userLabels[key];

            if(label) {
                error.message = label.message;
                status = label.status;
            }

            res.status(status).json(error);
        }
    };

    next();
}