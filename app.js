// Imports
require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const routes = require('./routes');
const views = require('./views');

// Database connect
mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

// Configs
const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Load views
views.map(view => app.use(view));

app.use(routes);

module.exports = app;
