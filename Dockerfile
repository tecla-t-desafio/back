FROM node:12
WORKDIR /app

## DEVELOPMENT
#CMD npm run dev

# PRODUCTION
COPY . .
RUN npm i
CMD npm start
